{- |
 - Module      :  Sorts
 - Description :  A collection of sorting algorithm implementations.
 - Copyright   :  (c) 2018-2019 William Tracy
 -
 - Maintainer  :  afishionado@gmail.com
 - Stability   :  experimental 
 - Portability :  portable 
 -
 - This module exports a single list named "sorts". Each entry is a tuple 
 - containing a sorting function with the signature Block->Block, a 
 - human-readable string identifying the algorithm, and an integer containing 
 - a suggested list size to demonstrate the function on.
 -
 - You may add your own algorithms by adding an appropriate entry to this list.
 - Note that the identifier string for each entry must be unique, as it is used
 - to generate file names.
 - -}
 
module Sorts (sorts) where

import Data.Bits
import Block
import Control.Exception.Base

-- | Compares and swaps (as needed) adjacent list entries. The direction in
-- which it traverses the list is determined by whether the index is wrapped in
-- an instance of Left or Right. If Left, it sorts from i down to 0. If Right,
-- it sorts from i to the end of the list.
cocktailPass::Block->Either Int Int->Block
cocktailPass block (Left i)
  | i >= (size block) - 1 = block
  | otherwise = cocktailPass update (Left (i+1))
  where 
    update = compareSwapNeighbors block i
cocktailPass block (Right i)
  | i == 0 = block
  | otherwise = cocktailPass update (Right (i-1))
  where 
    update = compareSwapNeighbors block i

-- | Cocktail sort works like bubble sort, but it alternates between passes in
-- ascending order and ones in descending order. 
-- | <https://en.wikipedia.org/wiki/Cocktail_shaker_sort>
cocktail::Block->Block
cocktail block =
  if (isDone block) then
    block
  else
    cocktail (cocktailPass (cocktailPass block (Left 0))
      (Right ((size block) - 2)))

-- | Repeatedly compares adjacent entries in the list. If they are in the 
-- incorrect order, they are swapped. This is repeated until the list is  
-- sorted.
-- | <https://en.wikipedia.org/wiki/Bubble_sort>
bubblesort::Block->Block
bubblesort block =
  if (isDone block) then
    block
  else
    bubblesort (cocktailPass block (Left 0))

-- | Calculate a new comb sort gap size by dividing the current gap size by 
-- a shrink factor. Uses the shrink factor of 1.3 originally proposed by 
-- Dobosiewicz.
combShrink::Int->Int
combShrink i = floor ((fromIntegral i) / 1.3)

-- | Compares the value at offset and the value gap steps farther down the 
-- list. Swaps them if appropriate, then recurses on to the next index.
combStep::Block->Int->Int->Block
combStep block gap offset 
  | offset + gap >= (size block) = block
  | otherwise = combStep update gap (offset + 1)
  where 
    update = compareSwap block offset (offset + gap)

-- | Makes one comb sort pass down the list with the given gap value. Repeats
-- recursively with a smaller gap value.
combPass::Block->Int->Block
combPass block gap 
  | gap <= 1 = bubblesort block 
  | otherwise = combPass (combStep block gap 0) (combShrink gap)

-- | Comb sort functions as a modified bubble sort. Instead of comparing 
-- adjacent values, it compares values separated by a certain gap. At the end 
-- of each pass, the gap size is reduced. This implementation starts with a gap
-- size equal to the size of the -- list (causing it to me immediately shrunk 
-- via combShrink).
-- | <https://en.wikipedia.org/wiki/Comb_sort>
comb::Block->Block
comb block = combPass block (size block)

-- | Calculate a new shell sort gap size by dividing the current gap size by 
-- a shrink factor. Uses the shrink factor of 2.2 proposed by Gonnet and 
-- Baeza-Yates.
shellShrink::Int->Int
shellShrink i = floor ((fromIntegral i) / 2.2)

-- | Compares the value at offset and the value gap steps farther down the 
-- list. Swaps them if appropriate, then recurses on to the next index.
shellStep::Block->Int->Int->Block
shellStep block gap offset 
  | gap + offset >= (size block) = block
  | otherwise = 
      shellStep (compareSwap block offset (offset + gap)) gap (offset + 1)

-- | Makes a pass down the list with the given gap size before recursing.
-- If any updates were made to the list during the pass, the recursive call
-- is made with the same gap size. Otherwise, the gap size is decreased.
shellPass::Block->Int->Block
shellPass block gap =
    if update == block then
      if gap > 1 then
        shellPass update (shellShrink gap)
      else
        update
    else
      shellPass update gap
  where update  = shellStep block gap 0

-- | Shell sort works like comb sort, but the gap size is not shrunk until the 
-- algorithm is able to traverse the entire list with the current gap size 
-- without encountering an incorrectly-ordered pair. Starts with a gap size 
-- equal to the size of the -- list (causing it to me immediately shrunk via 
-- shellShrink).
-- | <https://en.wikipedia.org/wiki/Shellsort>
shell::Block->Block
shell block = shellPass block (size block)

-- | Compares the value at the index with the following value. Swaps them if 
-- appropriate, then makes a recursive call. Moves backward if a switch was 
-- made, forward otherwise.
--
-- | The otherwise clause performs the comparison inline instead of calling
-- compareSwapNeighbors and checking the output for performance reasons.
gnomeStep::Block->Int->Block
gnomeStep block index
  | index >= ((size block) - 1) = block
  | index == 0 = gnomeStep (compareSwapNeighbors block 0) 1
  | otherwise =
    if (get block index) > (get block (index + 1)) then
      gnomeStep (swap block index (index + 1)) (index - 1)
    else
      gnomeStep block (index + 1)

-- | Gnome sort traverses a list until it encounters an out-of-order pair. The
-- smaller value is then walked backwards through the list until its correct 
-- location is found. The traversal is resumed from there.
-- | <https://en.wikipedia.org/wiki/Gnome_sort>
gnome::Block->Block
gnome block = gnomeStep block 0

-- | Returns the inde of the smallest value in the block whose index is >= i.
findSmallest::Block->Int->Int
findSmallest block i
  | i >= (size block) - 1 = i
  | otherwise =
    let 
      alternative = findSmallest block (i + 1) 
    in
      if (get block i) < (get block alternative) then
        i
      else
        alternative

-- | Swaps the value at i with the value that should be at i in the sorted 
selectionStep::Block->Int->Block
selectionStep block i = swap block i (findSmallest block i)

-- | Selection sort iterates over every list entry. For each entry, it searches
-- for the value that ultimately belongs there, then moves that value directly
-- to its destination.
-- | <https://en.wikipedia.org/wiki/Selection_sort>
selection::Block->Block
selection block = forEach block selectionStep

-- | Find the index of the parent for a given index.
heapParent::Int->Int
heapParent i = floor ((fromIntegral i) / 2)

-- | Find the index of the left child of a given index.
heapLeft::Int->Int
heapLeft i = 2 * i

-- | Find the index of the right child of a given index.
heapRight::Int->Int
heapRight i = 2 * i + 1

-- | If both x and y are less than length (they represent indices within the
-- active section of the array) then the index returned is the one referring
-- to the largest value. If one of those arguments is out of bounds, the other
-- index is immediately returned. Callers must not pass two out of bound
-- indeces simultaneously.
largerValue::Block->Int->Int->Int->Int
largerValue block length x y 
  | x >= length = assert (y < length) y
  | y >= length = assert (x < length) x
  | (get block x) > (get block y) = x
  | otherwise = y


-- | Evaluates the value of a given index, the value of both its children, and 
-- returns the largest of the three values.
largestSelfOrChildren::Block->Int->Int->Int
largestSelfOrChildren block i length =
  let
    l = heapLeft i
    r = heapRight i
  in
    largerValue block length r $ largerValue block length l i

-- | Rearranges i and its children until they satisfy the max heap property.
-- Specifically, if the largest of i's children is larger than i, then i and 
-- the larger child are swapped. maxHeapify is then repeated recursively on the
-- just-swapped child.
maxHeapify::Block->Int->Int->Block
maxHeapify block i length = 
  let
    largest = largestSelfOrChildren block i length
  in
    if (largest == i) then
      block
    else
      (maxHeapify (swap block i largest) largest length)
   
-- | Rearranges the block to satisfy the max-heap property.
buildMaxHeap::Block->Int->Int->Block
buildMaxHeap block i length
  | i <= 0 = block
  | otherwise = buildMaxHeap (maxHeapify block i length) (i - 1) length

-- | Assumes that indices from 0 through i-1 satisfy the max-heap property, and
-- that i through the end of the list are already sorted. Moves the largest
-- heap entry to the start of the already-sorted section of the array, restores
-- the max-heap property to the (now smaller) heap section of the list, and
-- repeats until the list is sorted.
heapsortPass::Block->Int->Block
heapsortPass block i
  | i <= 1 = block
  | otherwise = (heapsortPass (maxHeapify (swap block 0 i) 0 (i - 1)) (i - 1))

-- | First arranges the array into a max-heap, then extracts a sorted array 
-- from the heap.
-- <https://en.wikipedia.org/wiki/Heapsort>
heapsort::Block->Block
heapsort block = heapsortPass 
  (buildMaxHeap block (floor ((fromIntegral (size block)) / 2)) (size block)) 
  ((size block) - 1)

-- | Returns the given block unchanged. (The quickStep function below requires
-- a function that selects a pivot and moves it to the end of the section being
-- sorted. This function selects whatever value was already at the end of the
-- section as the pivot.)
pivotLast::Block->Int->Int->Block
pivotLast block _ _ = block

-- | Returns the block with the value midway between the start and end indices
-- swapped with the value and the end index. Using the midpoint of the segment
-- results in good running times for already-sorted or nearly-sorted lists.
pivotMidpoint::Block->Int->Int->Block
pivotMidpoint block start end = 
  swap block (floor ((fromIntegral (start + end)) / 2)) end

-- | Responsible for filtering values between the stand and end indices 
-- based on whether they are less then or greater than the pivot. When this is
-- complete, it calls quickPass on each ot these segments.
-- Pivot is a function that should select a pivot and move it to the end index.
-- The segment [start, end) is the segment being operated on.
-- The segment [start, i) should contain all values less than the pivot.
-- The segment [i, j) should contain all values greater than or equal to the 
-- pivot.
-- The value at [end] is the pivot.
quickStep::(Block->Int->Int->Block)->Block->Int->Int->Int->Int->Block
quickStep pivot block start end i j
  | j >= (end) = -- This pass is complete, recurse on the two segments
    quickPass 
      pivot 
      (quickPass pivot (swap block i end) start (i - 1)) 
      (i + 1) 
      end
  | (get block j) < (get block end) = -- add element to block one
    let 
      swapped = compareSwap block i j
    in
      quickStep pivot swapped start end (i + 1) (j + 1)
  | otherwise =  -- add element to block two
    quickStep pivot block start end i (j + 1)

-- | Responsible for setting up a segment to sort via quickStep.
quickPass::(Block->Int->Int->Block)->Block->Int->Int->Block
quickPass pivot block start end  
  | end <= start = block
  | otherwise = quickStep pivot (pivot block start end) start end start start

-- | Repeatedly divides an array into two segments. Elements larger than a 
-- certain value (called a pivot) are placed into the second part, and those
-- less than the pivot are placed in the first part.
-- The pivot function is responsible for choosing a pivot. It should choose a
-- value from an index between the provided start and end indeces, and move it
-- to the end index.
-- This implementation is based on Lomuto scheme, with pluggable implementations
-- for pivot selection. 
-- <https://en.wikipedia.org/wiki/Quicksort>
quick::(Block->Int->Int->Block)->Block->Block
quick pivot block = quickPass pivot block 0 ((size block) - 1)

-- | Responsible for filtering values between the stand and end indices 
-- based on whether they are less then or greater than the pivot. When this is
-- complete, it calls quickPass on each ot these segments.
-- Pivot is a function that should select a pivot and move it to the end index.
-- The segment [start, end) is the segment being operated on.
-- The segment [start, i) should contain all values less than the pivot.
-- The segment [j, end) should contain all values greater than or equal to the 
-- pivot.
-- The value at [end] is the pivot.
hoareQuickStep::(Block->Int->Int->Block)->Block->Int->Int->Int->Int->Int->Block
hoareQuickStep pivot block pVal start end i j
  | i >= j = hoareQuickPass 
      pivot 
      (hoareQuickPass 
        pivot 
        (swap block i end)
        start 
        (i-1)) 
      (i+1) 
      end
  | (get block i) < (get block pVal) = 
    hoareQuickStep pivot block pVal start end (i + 1) j
  | (get block j) >= (get block pVal) = 
    hoareQuickStep pivot block pVal start end i (j - 1)
  | otherwise = 
    hoareQuickStep pivot (swap block i j) end start end (i + 1) (j - 1)

-- | Responsible for setting up a segment to sort via hoareQuickStep.
hoareQuickPass::(Block->Int->Int->Block)->Block->Int->Int->Block
hoareQuickPass pivot block start end  
  | end <= start = block
  | otherwise = 
    hoareQuickStep pivot (pivot block start end) end start end start end

-- | Hoare-style quicksort. Partitions each segment working inward from both 
-- ends. Otherwise, all comments on the Lomuto-style quicksort implementation 
-- apply here.
hoareQuick::(Block->Int->Int->Block)->Block->Block
hoareQuick pivot block = hoareQuickPass pivot block 0 ((size block) - 1)

-- | Returns the first index after "incision" whose value is less than or equal
-- to the value at "next". If the list is in sorted order up to the next index,
-- then this is the location that next can be inserted while maintaning the 
-- ordering.
findInsertionPoint::Block->Int->Int->Int
findInsertionPoint block incision next
  | incision == next = incision
  | (get block incision) < (get block next) = 
    findInsertionPoint block (incision + 1) next
  | otherwise = incision

-- | If the array indices before next are in sorted order, this function moves
-- the value from next forward in the list to bring it in sorted order.
insertStep::Block->Int->Block
insertStep block next = move block next (findInsertionPoint block 0 next)

-- | Performs a naive insertion sort. A sorted sub-list is created at the 
-- start of the block, and subsequent values are inserted into the sorted list 
-- at the correct locations.
-- <https://en.wikipedia.org/wiki/Insertion_sort>
insertion::Block->Block
insertion block = forEach block insertStep

-- | Assumes that the range [start, end) is sorted except for next, and that 
-- start <= next <= end. Returns immediately if next is in the correct 
-- location. If not, it calls binaryInsert. The value of next will be 
-- preserved, but either start or end will be moved closer to next, reducing 
-- the size of the range.
bisectSearchSpace::Block->Int->Ordering->Int->Int->Block
bisectSearchSpace block next EQ start end = block
bisectSearchSpace block next LT start end = binaryInsert block next start next
bisectSearchSpace block next GT start end = binaryInsert block next next end

-- | Assumes that the range [start, end) is sorted except for next, and that 
-- start <= next <= end. Returns immediately if next is in the correct 
-- location. If not, next is moved forward or backward through the block until 
-- all entries in the range [start, end] are in sorted order.
binaryInsert::Block->Int->Int->Int->Block
binaryInsert block next start end 
  | start == end = block
  | end - start == 1 = compareSwapNeighbors block start
  | otherwise =
  let
    midpoint = (floor ((fromIntegral (start + end)) / 2))
    old = get block midpoint
    new = get block next
    comparison = compare new old
    intermediate = move block next midpoint
  in
    bisectSearchSpace intermediate midpoint comparison start end

-- | Assumes that the range [0, next) is sorted. Inserts next into the correct 
-- location so that the range [0, next] is ordered.
binaryInsertionStep::Block->Int->Block
binaryInsertionStep block next = binaryInsert block next 0 next

-- | Performs a binary insertion sort. Performs an insertion sort using binary 
-- search to find the correct insertion points.
-- <https://www.geeksforgeeks.org/binary-insertion-sort/>
binaryInsertion::Block->Block
binaryInsertion block = forEach block binaryInsertionStep

-- | Sorts the range [startA, end) by merging the ranges [startA, startB) and 
-- [startB, end). Assumes that the [startA, startB) and 
-- [startB, end) ranges are already sorted, and that startA <= startB <= end.
merge::Block->Int->Int->Int->Block
merge block startA startB end
  | startA == startB = block
  | startB == end = block
  | get block startA > get block startB = 
    merge (move block startB startA) (startA + 1) (startB + 1) end
  | otherwise = merge block (startA + 1) startB end

-- | Sorts the range [start, end). First divides the range into two sub-ranges.
-- Recursively sorts those ranges, then interleaves them in sorted order.
divideAndMerge::Block->Int->Int->Block
divideAndMerge block start end
  | end - start <= 1 = block
  | otherwise = let 
      midpoint = floor ((fromIntegral start + fromIntegral end)/2) 
      intermediate = 
        divideAndMerge (divideAndMerge block start midpoint) midpoint end
    in
      merge intermediate start midpoint end

-- | Performs a depth-first (top-down) merge sort. Recursively divides the 
-- list, sorts the sub-lists, then interleaves the sub-lists in sorted order.
-- <https://en.wikipedia.org/wiki/Merge_sort#Top-down_implementation>
mergeSort::Block->Block
mergeSort block = divideAndMerge block 0 (size block)

-- | Performs a merge sort over the range [start, start + size)
mergeSortStep size block start = 
  merge block start (start + (floor ((fromIntegral size) / 2))) (start + size)

-- | Divides the block into sublists of size sublistSize (truncating the last 
-- one if necessary) and sorts each sublist. Recursively calls itself with a 
-- larger sublist size, until the sublist size is larger than the entire block.
mergeSortBreadthPass block sublistSize
  | sublistSize > size block = block
  | otherwise = 
    mergeSortBreadthPass 
      (forEachStep block (mergeSortStep sublistSize) sublistSize)
      (sublistSize * 2) 

-- | Performs a breadth-first (bottom-up) merge sort. Creates small sublists, 
-- and interleaves them in sorted order to create increasingly larger sorted 
-- sublists.
-- <https://en.wikipedia.org/wiki/Merge_sort#Bottom-up_implementation>
mergeSortBreadth::Block->Block
mergeSortBreadth block = mergeSortBreadthPass block 1

-- | Performs one step of an MSD Radix sort. Decreases the size of the segment 
-- not yet sorted by the current digit by one. If there is more to sort, it 
-- recursively calls itself.
--  | digit contains the number of the bit currently being used to partition 
--  the input. The range [start, zEnd) should contain all values with this bit 
--  cleared. (zEnd is the end of the zeroes segment.)  The range [zEnd, oEnd) 
--  should contain all values with this bit set. (oEnd marks the end of the 
--  ones segment.) The range [oEnd, end) contains all unsorted values.
msdStep::Block->Int->Int->Int->Int->Int->Block
msdStep block digit start zEnd oEnd end 
  | oEnd >= end = msdPass (msdPass block digit start zEnd) digit zEnd end
  | (testBit (get block oEnd) digit) = 
    msdStep block digit start zEnd (oEnd + 1) end
  | otherwise = 
    msdStep (swap block zEnd oEnd) digit start (zEnd + 1) (oEnd + 1) end

-- | Partitions the block by the bit given in digit. If there is more sorting 
-- to be done, calls itself recursively.
msdPass::Block->Int->Int->Int->Block
msdPass block digit start end
  | digit <= 0 = block
  | otherwise = msdStep block (digit - 1) start start start end

-- | Most Significant Digit Radix sort. Partitions the values based on whether 
-- the first bit is cleared or set. Then recursively divides each partition by 
-- the second digit, and so on.
-- <https://en.wikipedia.org/wiki/Radix_sort#Most_significant_digit_radix_sorts>
radixMSD::Block->Block
radixMSD block = msdPass block 8 0 (size block)

-- | Incrementally partitions the data by the current bit, then calls itself 
-- recursively. The range [0, zEnd) has all values with the current bit 
-- cleared. (zEnd is the end of the zeroes partition.) The range [zEnd, oEnd) 
-- contains all values with the current bit set. (oEnd marks the end of the 
-- ones partition.) Unsorted values occupy indices > oEnd.
lsdStep block digit zEnd oEnd
  | oEnd >= (size block) = block
  | (testBit (get block oEnd) digit) = lsdStep block digit zEnd (oEnd + 1)
  | otherwise = lsdStep (move block oEnd zEnd) digit (zEnd + 1) (oEnd + 1)

-- | Partitions the data based on the value of the current bit in each entry, 
-- then recursively moves on to the next bit.
lsdPass::Block->Int->Block
lsdPass block digit 
  | digit >= 8 = block
  | otherwise = lsdPass (lsdStep block digit 0 0) (digit + 1)

-- | Least Significant Digit Radix sort. Recursively partitions the dataset 
-- by the value of a given bit, starting with the least significant bit.
radixLSD::Block->Block
radixLSD block = lsdPass block 0 

-- | Essentially binaryInsertion with the ability to specify a starting and 
-- ending point. Uses a less-elegant algorithm to achieve this.
buildRun block start current end 
  | current >= end = block
  | otherwise = 
    buildRun (binaryInsert block current start current) start (current + 1) end

-- | Finds the end of any run of monotonically increasing values starting at 
-- start. (Specifically, it returns the first index not part of that run.) If 
-- the run spans the entire range [start, end), then the function bails out 
-- and returns end.
findRunEnd::Block->Int->Int->Int
findRunEnd block start end
  | start >= end - 1 = end
  | (get block start) > (get block (start + 1)) = (start+1)
  | otherwise = findRunEnd block (start + 1) end

-- | Traverses the data. Uses insertion sort to build runs of monotonically 
-- ascending values of length 32 or greater.
buildRuns::Block->Int->Int->Block
buildRuns block current end
  | current >= end = block
  | otherwise = 
  let
    initialRunEnd = findRunEnd block current end
    intermediate = 
      buildRun block current initialRunEnd (min end (current + 32))
    runEnd = findRunEnd intermediate current end
  in
    (buildRuns intermediate runEnd end)

-- | Examines any run of monotonically ascending values starting at the 
-- current index. If there is not a run of the minimum length present, it 
-- builds one.
prepRun block minimum current end =
  let
    runEnd = findRunEnd block current end
    runLength = runEnd - current
  in
    if runLength >= minimum || runEnd >= end
      then block
      else prepRun (mergeAdjacentRuns block current end) minimum current end

-- | Looks for a run of monotonically ascending values from current. Then 
-- looks for a second run of equal length. If one does not exist, it builds 
-- one. Then, it merges the two runs.
mergeAdjacentRuns block current end 
  | current >= end = block
  | otherwise =
    let
      runEndA = findRunEnd block current end
      lengthRunA = runEndA - current
      prepped = prepRun block lengthRunA runEndA end
      runEndB = findRunEnd prepped runEndA end
    in
      merge prepped current runEndA runEndB

-- | Recursively merges runs of monotonically ascending values 
-- until the set is sorted.
mergeAllRuns block 
   | (isDone block) = block
   | otherwise = mergeAllRuns (mergeAdjacentRuns block 0 (size block))

-- | Tim sort. Adaptively uses insertion sort to build runs of ascending 
-- values, then performs a merge sort on those runs.
-- | First, buildRuns uses insertion sort to generate runs of a minimum 
-- length. Once all values are a member of one of these runs, mergeAllRuns 
-- uses a natural merge sort to merge them until one run encompasses the whole
-- set.
-- <https://en.wikipedia.org/wiki/Timsort>
tim::Block->Block
tim block = mergeAllRuns (buildRuns block 0 (size block))

-- | Compares (and swaps if appropriate) the values at the start and end 
-- indices. Then recursively calls self first on the first two-thirds of the 
-- range, then on the last two-thirds, and then of the first two-thirds again.
doStooge::Block->Int->Int->Block
doStooge block start end 
  | range <= 2 = buildRun result start start end
  | otherwise = doStooge (doStooge (doStooge result start m2) m1 end) start m2
  where 
    range = end - start
    m1 = start + floor (fromIntegral range * 1 / 3)
    m2 = start + ceiling (fromIntegral range * 2 / 3)
    result = compareSwap block start end

-- | Stooge sort, named after the three stooges. Sorts from the ends, works 
-- its way inward, then works its way outward again.
-- <https://en.wikipedia.org/wiki/Stooge_sort>
stooge::Block->Block
stooge block = 
  doStooge block 0 (size block - 1)

-- | Compares the value at start to the following value, and swaps if 
-- appropriate. Then repeats the process with the following pair.
evenOddStep::Block->Int->Int->Block
evenOddStep block start length
  | length - start <= 1 = block
  | otherwise = 
    evenOddStep (compareSwapNeighbors block start) (start + 2) length

-- | Even-odd sort is similar to bubblesort. However, it makes alternating 
-- passes first comparing and swapping pairs whose first entry is an even 
-- index, then comparing and swapping pairs whose first entry is an odd index.
-- <https://en.wikipedia.org/wiki/Odd%E2%80%93even_sort>
evenOdd::Block->Block
evenOdd block = 
  let
    length = size block
  in
    if isDone block
      then block
      else evenOdd $ evenOddStep (evenOddStep block 0 length) 1 length

-- | Register your new sorting function here. The first argument in each tuple
-- is the sorting function. The second argument is a string that uniquely 
-- identifies your sorting function. The last is an integer indicating the size
-- of the input set to run your function against. The framework has not been 
-- tested with sizes greater than 256, and trying it is not recommended.
sorts::[((Block->Block), String, Int)]
sorts = [
  ((hoareQuick pivotLast), "quicksort.hoare.last", 256),
  ((hoareQuick pivotMidpoint), "quicksort.hoare.midpoint", 256),
  ((quick pivotLast), "quicksort.last", 256),
  ((quick pivotMidpoint), "quicksort.midpoint", 256),
  (gnome, "gnomesort", 128),
  (evenOdd, "evenodd", 64),
  (stooge, "stoogesort", 64),
  (tim, "timsort", 256),
  (radixLSD, "radix.lsd", 256),
  (radixMSD, "radix.msd", 256),
  (mergeSort, "mergesort.depthfirst", 256),
  (mergeSortBreadth, "mergesort.breadthfirst", 256),
  (bubblesort, "bubblesort", 128),
  (cocktail, "cocktailsort", 128),
  (comb, "combsort", 256),
  (shell, "shellsort", 256),
  (selection, "selectionsort", 256),
  (heapsort, "heapsort", 128),
  (insertion, "insertionsort", 256),
  (binaryInsertion, "binaryinsertionsort", 128)
  ]
