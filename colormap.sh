#!/bin/bash

# Applies the specified palette to all PNG images in the current directory, and 
# saves the result under a directory named colormap. Expects all images in the 
# working directory to be grayscale.

mkdir -p colormap

for file in *.png
do
  echo $file
  convert $file palettes/bluegold.png -clut colormap/$file
done
