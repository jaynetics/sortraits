#!/bin/bash

# Generates polar-coordinate transformations of raw and difference 
# visualizations.

mkdir -p polar/difference

# We need something interesting to fill the space around the circle-shaped 
# polar visualization. We'll fill it with a set of sorted values extending to 
# the image edges. The line below generates such a gradient, converts it to 
# polar coordinates at 2048x2048 resolution, and crops it down to 1024x1024, 
# eliminating the black dead zones. The result appears to extend to infinity.
echo "Generating template..."
convert -size 2048x2048 -define gradient:direction=West gradient: \
  -virtual-pixel HorizontalTile -background Black -distort Polar 0 +repage \
  -crop 1024x1024+512+512 temp.png

# Perform the polar transform on available images, and composite them over the 
# "template" we just created.
for file in *.png
do
  echo $file
  convert $file -scale 1024x1024\! +repage -virtual-pixel HorizontalTile \
    -background Transparent +distort Polar 0 +repage polar/$file
  composite -compose over polar/$file temp.png polar/$file
done

# Convert the difference visualizations. Since sorted segments show up black, 
# extending outward with black isn't noticeably "wrong" and we can skip the 
# compositing step.
for file in difference/*.png
do
  echo $file
  size="$(identify -format %wx%h $file)"
  convert $file -scale 1024x100% +repage -virtual-pixel HorizontalTile \
    -background Black +distort Polar 0 +repage polar/$file
done

rm temp.png
