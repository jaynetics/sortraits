# Sortraits: Portraits of Sorting Algorithms

This is the documentation for developers interested in modifying or adding to the Sortraits project. If you just want to look at pretty pictures, visit our main page at: https://wtracy.gitlab.io/sortraits/

This codebase is an unholy union of Haskell code and Bash shell scripts. The management does not assume any responsibility for your sanity should you continue further.

## Building and Running

To build the Haskell code, execute `ghc -threaded Sortraits` from the project directory. Running the resulting `Sortraits` executable will generate the images.

There are several shell scripts that perform different postprocessing passes on the output from the Haskell code. They are all designed to be run from the project root directory. Each generates and populates and subdirectory named after itself. `difference.sh` generates difference visualizations, where each pixel is colored based on the difference between the current value and the expected value. `huediff.sh` and `overlay.sh` both operate on the output from `difference.sh`.

To debug a sorting method, load Sorts.hs into ghci (execute GHCI, then enter the command `:l Sorts.hs`). Set your breakpoints, then invoke your function with:

`yourfunc (start [1, 2, 3])`

Modify the list as appropriate. If you are debugging one of the quicksorts, you will need to provide a pivot function, for example:

`(quick pivotLast) (start [1, 2, 3])`

## The Codebase

The Haskell code consists of three modules. `Block.hs` defines primitives used by the sorting functions. `Sorts.hs` contains all the sorting implementations. `Sortraits.hs` contains the execution harness for the sorts.

timsort.gv includes a control flow graph of the Timsort implementation in Sorts.hs. You can view it with the command: `dot -Tpng timsort.gv|feh -`

## Todo

✓ Document and clean up everything.

✓ Implement Hoare-style quicksort. (Still need to document/clean.)

Maybe implement smooth sort. Probably not gravity sort. Not sure about patience sort, Dutch-flag sort, American-flag sort, and block sort.

✓ Experiment with polar-coordinate-based visualizations.

Experiment with partitioning values in output as unmodified, modified, and final values. Each partition could be color-coded differently.

✓ Find better way of doing rainbows. Specifically, offsetting the hues so that red is not at zero.
