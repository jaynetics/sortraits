#!/bin/bash

# Writes images under a directory named difference in which dark pixels 
# represent values at or close to their final position, and bright pixels 
# represent values far away from their final position.
#
# How it works: For every image in the working directory: Generates a temporary 
# image with the same dimensions, populated with all values as they would be in 
# the sorted list. Composites the input and temporary image with a difference 
# function.

mkdir -p difference

for file in *.png 
do
  echo $file
  size="$(identify -format '%wx%h' $file)"
  convert -size $size -define gradient:direction=West gradient: difference/temp.png
  composite $file difference/temp.png -compose difference difference/$file
done

rm difference/temp.png
