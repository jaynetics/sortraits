#!/bin/bash

# Buggy/unmaintained.
#
# Generates files under the overlay directory that pull one channel from the 
# original visualization, and other channels from the difference visualization.

mkdir -p overlay

for file in *.png
do
  echo $file
  convert $file difference/$file -background black -channel BG -combine overlay/$file
  #gm composite -compose CopyBlue $file  difference/$file overlay/$file
done
