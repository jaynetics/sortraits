{- |
 - Module      :  Sortraits
 - Description :  An execution harness for the various sorting functions
 - Copyright   :  (c) 2018-2019 William Tracy
 -
 - Maintainer  :  afishionado@gmail.com
 - Stability   :  experimental 
 - Portability :  portable 
 -
 - This module is responsible for setting up the various input sets to be 
 - sorted, executing the sorts, and writing the output images.
 - -}
import Data.ByteString.Lazy as BS
import Data.Either
import Control.Parallel
import Control.Parallel.Strategies
import Control.Concurrent
import Control.Exception
import System.IO as IO
import System.Random
import System.Random.Shuffle

import Codec.Picture

import Block
import Sorts

-- | Color depth of the output image.
depth::Int
depth = 256

-- | The largest value that a Pixel8 can have.
maxValue::Pixel8
maxValue = (fromIntegral depth) - 1

-- | Produces a list of values already in sorted order. If the requested list 
-- length is equal to depth, the output contains exactly one of every 
-- possible output. If shorter, it attempts to produce a list containing 0, 
-- maxValue, and a range of values between them that are as evenly spaced as 
-- possible.
ordered::Int->[Pixel8]
ordered n = Prelude.map 
  (fromIntegral . floor) 
  [0, ((fromIntegral maxValue)/(fromIntegral n-1))..(fromIntegral maxValue)]

-- | Starts by using ordered to produce an ordered list. Then removes the 
-- first half of the list, and appends it to the end of the list.
swapped::Int->[Pixel8]
swapped n =
  let
    original = ordered n
    break = floor ((fromIntegral n) / 2)
  in
    (Prelude.drop break original) ++ (Prelude.take break original)

-- | Starts by using ordered to produce an ordered list. The removes the
-- first and last thirds of the list. Adds the original final third at the 
-- front of the list, and adds the original first third at the end of the list.
swappedThirds::Int->[Pixel8]
swappedThirds n =
  let
    original = ordered n
    oneThird = floor (fromIntegral n / 3)
    twoThirds = 2 * oneThird
  in
    (Prelude.drop twoThirds original) 
      ++ (Prelude.take oneThird (Prelude.drop oneThird original)) 
      ++ (Prelude.take oneThird original)

-- | Produces a list by interleaving values from two input lists.
interleave::[a]->[a]->[a]
interleave [] y = y
interleave (n:x) y = n : (interleave y x)

-- | Starts by using ordered to produce an ordered list. Then splits the list 
-- in half, and interleaves values from the two halves. If you traverse the 
-- output, you will find that values alternate between those from the original 
-- first half of the list, and those from the original last half.
interleaved::Int->[Pixel8]
interleaved n =
  let
    original = ordered n
    break = floor ((fromIntegral n) / 2)
  in
    interleave (Prelude.take break original) (Prelude.drop break original)

-- | Starts with the output from ordered, and shuffles it into a random order.
shuffled::Int->Int->[Pixel8]
shuffled n seed =
  (shuffle' (ordered n) n (mkStdGen seed))

-- | Same as ordered, but with output in reversed order.
reversed::Int->[Pixel8]
reversed n = Prelude.reverse (ordered n)

-- | If n is in the range [0, 4), the output is normalized to span the range 
-- [0, 256).
normalize::Pixel8->Pixel8
normalize n = floor (fromIntegral (255 * fromIntegral n) / 4)

-- | Produces a list containing a random combination of four values.
repeating::Int->Int->[Pixel8]
repeating n seed = Prelude.map 
  normalize 
  (Prelude.take n (randomRs (0, 3) (mkStdGen seed)))

-- | Given a sorting function and an input list, executes the sort, captures
-- the result, and encodes it as a PNG image.
sortAndEncode::(Block->Block)->Block->(Either String BS.ByteString)
sortAndEncode sort block = 
    (encodeDynamicPng (exportImage (sort block)))

-- | Takes a sort function, human-readable name for that function, and input 
-- size. Produces six possible inputs for the given size, and executes the 
-- sort on each one. The result is a list of tuples. The first entry in each 
-- tuple is the PNG-encoded output. The second is a suggested filename.
-- | This would be a good target for refactoring. Perhaps there should be a 
-- list of input generating functions and associated file suffixes, and this 
-- function should just iterate through them?
setupSort::((Block->Block), String, Int)->
  [((Either String BS.ByteString), String)]
setupSort (sort, name, size) = [
    ((sortAndEncode sort (start (shuffled size 2))), 
      (name ++ ".shuffled.png")),
    ((sortAndEncode sort (start (reversed size))), (name ++ ".reversed.png")),
    ((sortAndEncode sort (start (ordered size))), (name ++ ".ordered.png")),
    ((sortAndEncode sort (start (swapped size))), 
      (name ++ ".swapped.halves.png")),
    ((sortAndEncode sort (start (interleaved size))), 
      (name ++ ".interleaved.png")),
    ((sortAndEncode sort (start (swappedThirds size))), 
      (name ++ ".swapped.thirds.png"))
  ]

-- | Attempts to write the provided image to the provided file name. If the 
-- data is a normal String, it is assumed to be an error message and is output 
-- to stdout. If it is a ByteString, it is assumed to be valid data and is 
-- written to file.
writeImage::((Either String BS.ByteString), String)->IO()
writeImage ((Left error), name) = do 
  IO.putStr "Cannot write "
  IO.putStr (name)
  IO.putStr ": "
  IO.putStrLn error
writeImage ((Right body), name) = do
  IO.putStr "Writing "
  IO.putStr name
  IO.putStrLn "..."
  BS.writeFile name body

-- | Sets up all sorting/file writing jobs and executes them in parallel.
main::IO()
main = let
    jobs = Prelude.foldl (++) [] (Prelude.map setupSort sorts)
    pJobs = jobs `using` (parTraversable rdeepseq)
  in do
    mapM_ writeImage pJobs


