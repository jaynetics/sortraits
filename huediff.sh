#!/bin/bash

# Similar to colormap.sh, but takes input from the difference directory and 
# outputs to the huediff directory.

mkdir -p huediff

for file in *.png
do
  echo $file
  convert difference/$file palettes/rainbow3.png -clut huediff/$file
done
